\begin{code}
{-# OPTIONS_GHC -fglasgow-exts  #-}

module Main where

import Control.Monad
import Control.Monad.State
import Control.Monad.Trans
import Control.Monad.Error
import Control.Arrow

import Text.ParserCombinators.Parsec hiding (State)

import Data.Set (Set)
import qualified Data.Set as Set
import Data.Map (Map)
import qualified Data.Map as Map
import Data.List
import Data.Maybe

import Debug.Trace
\end{code}

\begin{code}
type StateWithFail s e a =  ErrorT e (State s) a

evalSWF :: StateWithFail s e a -> s -> Either e a
evalSWF swf s = evalState (runErrorT swf) s

runSWF :: StateWithFail s e a -> s -> (Either e a, s)
runSWF swf s = runState (runErrorT swf) s

instance Error () where
  noMsg    = ()
  strMsg _ = ()

instance Error Clause where
  noMsg    = undefined
  strMsg   = undefined

type Variable = Int
data CNF = And [Clause] deriving (Show, Eq)
data Clause = Or [Literal] deriving (Show, Eq)
type Literal = (Bool, Variable)
type Decision = (Bool, Variable)
type WatchList = Map Literal [Clause]
data Env = Env (Map Variable Bool) [Variable]
  deriving (Show)

class WatchListC w where
  initWL :: CNF -> w
  appendWatches :: Literal -> [Clause] -> w -> w
  appendWatch :: Literal -> Clause -> w -> w
  clearWatch :: Literal -> w -> w
  lookupWatch :: Decision -> w -> [Clause]
  updateWatch :: Literal -> Clause -> Clause -> w -> w

class EnvC e where
  empty :: [Variable] -> e
  bindVar :: Variable -> Bool -> e -> e
  lookupVar :: Variable -> e -> Maybe Bool
  decide :: e -> Maybe Variable
\end{code}

\begin{code}
instance EnvC Env where
  empty = Env (Map.empty)
  bindVar v b (Env a ua) = Env (Map.insert v b a) (delete v ua)
  lookupVar v (Env a _) = Map.lookup v a
  decide (Env _ uas) = listToMaybe uas

litValue :: EnvC a => a -> Literal -> Maybe Bool
litValue e (bar, v) = (/= bar) `fmap` lookupVar v e
\end{code}

\begin{code}
instance WatchListC WatchList where
  initWL (And cs) = foldl' addReps Map.empty cs
    where
    addReps :: WatchList -> Clause -> WatchList
    addReps wl c@(Or (l1:l2:lits)) = appendWatch l1 c (appendWatch l2 c wl)
  appendWatches lit cs wl = Map.insertWith (flip (++)) lit cs wl
  appendWatch lit c wl = Map.insertWith ((:) . head) lit (return c) wl
  clearWatch lit wl = Map.delete lit wl
  lookupWatch d wl = maybe [] id $ Map.lookup d wl
  updateWatch lit oldc c wl = wl' 
    where
    oldcs = maybe [] id (Map.lookup lit wl)
    cs = c : (delete oldc oldcs)
    wl' = Map.insert lit cs (Map.delete lit wl)
\end{code}

\begin{code}
dpll :: CNF -> [Variable] -> Maybe Env
dpll cnf vars = either (const Nothing) Just $ evalSWF (dpll' (empty vars)) (initWL cnf)
  where
  dpll' :: Env -> StateWithFail WatchList () Env
  dpll' e = do
    wl <- get
    maybe (return e) (\v -> msum [tryDecision e (True, v), tryDecision e (False, v)]) $ trace "decide" (decide e)
     
  tryDecision :: Env -> Decision -> StateWithFail WatchList () Env
  tryDecision e (b, v) = do
    wl <- get
    --let (ee, wl') = bcp wl e (b, v) in put wl' >> either (const $ throwError ()) dpll' ee
    let (ee, wl') = runState (bcpM e v b) wl in put wl' >> either (const $ throwError ()) dpll' ee
\end{code}

\begin{code}
bcpM :: (EnvC e, WatchListC w) => e -> Variable -> Bool -> State w (Either Clause e)
bcpM e v b = 
  do
    wl <- get
    let cs = trace "prop" (lookupWatch (b, v) wl) in
      case runSWF (mapM (checkClauseM e' (b, v)) cs) (clearWatch (b, v) wl) of
        (Left c, wl')   -> do put $ addOldWatches (b, v) c cs wl'
                              return $ Left c
        (Right ws, wl') -> do put wl'
                              uncurry (flip (>>)) ((return *** put) (foldl' checkImpl (Right e', wl') ws))
  where
    e' = bindVar v b e
    checkImpl (Left c, wl'') _                    = (Left c, wl'')
    checkImpl ee Nothing                          = ee
    checkImpl (Right e'', wl'') (Just (b'', v'')) = runState (bcpM e'' v'' (not b'')) wl''

    addOldWatches lit c cs wl' = appendWatches lit (drop 1 $ dropWhile (/= c) cs) wl'

-- Left clause        ==> Conflict
-- Right Nothing      ==> Continue -- not unit
-- Right (Just lit)   ==> Continue -- unit
checkClauseM :: (EnvC e, WatchListC w) 
             => e -> Decision -> Clause -> StateWithFail w Clause (Maybe Literal)
checkClauseM e d c = do
  wl <- get
  let (w1, w2, lits) = case c of
                         Or (lit1:lit2:lits) | d == lit1 -> (d, lit2, lits)
                                             | otherwise -> (d, lit1, lits)
      isSat = fromMaybe False $ litValue e w2
      findLit = find (\lit -> litValue e lit /= Just False) lits
      computeUnit w = maybe (return (Just w))
                            (\b -> if b then return Nothing else (trace "conflict" (throwError c))) $ litValue e w
      swapWatches lit awl = awl''
        where
        c' = (Or (lit:w2:w1:delete lit lits))
        awl' = updateWatch w2 c c' awl
        awl'' = appendWatch lit c' awl'
    in
      if isSat
        then put (appendWatch d c wl) >> return Nothing
        else case findLit of
               Just wi -> put (swapWatches wi wl) >> return Nothing
               Nothing -> put (appendWatch d c wl) >> computeUnit w2

\end{code}

\begin{code}
bcp :: (EnvC e, WatchListC w) => w -> e -> Decision -> (Either e e, w)
bcp wl e d = 
  case conflict of
    True  -> (Left e', wl')
    False -> foldl' checkImpls (Right e', wl') impls
  where
  (db, dv) = d
  e' = bindVar dv db e
  cs = lookupWatch d wl
  (wl', cs') = mapAccumL checkClause (clearWatch d wl) cs
  
  conflict = Left False `elem` cs'
  impls = [(not bar, v) | Right (bar, v) <- cs']
  checkImpls ((Left e''), wl'')  _ = ((Left e''), wl'')
  checkImpls ((Right e''), wl'') ri = bcp wl'' e'' ri
   
  checkClause :: WatchListC w => w -> Clause -> (w, Either Bool Literal)
  checkClause wl c = 
    if isSat
      then (appendWatch d c wl, Left True)
      else case newLit of
             Just wi -> (adjWatches wi wl, Left True)
             Nothing -> (appendWatch d c wl, newUnit w2)
    where
    (Or (lit1:lit2:lits)) = c
    (w1, w2) = if lit1 == d then (lit1, lit2) else (lit2, lit1)

    isSat =  (maybe False id $ litValue e' w2)
    newLit = find (\lit -> litValue e' lit /= Just False) lits
    newUnit w = maybe (Right w) Left $ litValue e' w
    newClause w oldw  = (Or ((w:w2:delete w lits) ++ [oldw]))
    adjWatches newLit awl = bothFixed
      where
      c' = newClause newLit w1
      w2Fixed = updateWatch w2 c c' awl
      bothFixed = appendWatch newLit c' w2Fixed
\end{code}

\begin{code}
main = interact checkSat
  where
  checkSat :: String -> String
  checkSat s = 
    case parse parseDIMACS "" s of
      Left err       -> "parse error at " ++ show err
      Right (cnf, v) -> maybe "UNSAT\n" (const "SAT\n") $ dpll cnf [1..v]
\end{code}

\begin{code}
---
--- Parser
---

parseDIMACS :: Parser (CNF, Int)
parseDIMACS = do parseComments
                 (v, c) <- parseHeader
                 cnf <- liftM And parseClauses
                 return (cnf, v)

parseComments :: Parser [String]
parseComments = many $ char 'c' >> manyTill anyChar newline

parseHeader :: Parser (Int, Int)
parseHeader = do string "p cnf"
                 many1 space
                 vars <- many1 digit
                 many1 space
                 clauses <- many1 digit
                 many (char ' ')
                 newline
                 return (read vars, read clauses)

parseClauses :: Parser [Clause]
parseClauses = sepEndBy1 parseLiteral space >>= return . map Or . split 
  where
  split [] = []
  split xs = let (c, (z:xs')) = break (== (False, 0)) xs in c : split xs'

parseLiteral :: Parser Literal
parseLiteral = many space >> choice [char '-' >> parseInt True, parseInt False]
               where 
               parseInt polar = many1 digit >>= return . (,) polar . read
\end{code}

